/* foo.c -- an example C file header
 *
 * Copyright (C) 2018 Daniel Bicek
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */



#include <stdlib.h>
#include "math.h"
#include<SDL2/SDL.h>
#include "time.h"
#include <unistd.h>
#include <sys/syscall.h>
#include <linux/random.h>
#include "genann.h"


#define WINDOW_WIDTH 1000
#define WINDOWS_HEIGHT 1000

//Opts
int viewLen;
int nnInputSize;
int nnLayerCount;
int nnLayerSize;
int seed;
int spawnDistance;
int splitPoint;
int renderEvery;
int respwanDelay;
int gLearnLimit;
int learnigrate;
int showBodys;

//Stats
int SnakesLivedAll = 0;
int longestSnake = 0;

SDL_Renderer *renderer;
genann** ann;

unsigned int* randBuf = NULL;
unsigned int* randBufCur = NULL;
size_t bufSize = 1024 *1024;
int fc = 0;


char pointMap[1000000];

char overlay[1000000];

void createChessOverlay() {
   memset(overlay,0,1000000);
    int i;
    for (i = 0;i < 1000000;i++){
       if (i / 10 % 51 == 0 || i % 76 == 0) overlay[i] = 1;
    }
}


/*
int rand(){

    if (randBuf == NULL ) {
        randBuf = malloc(bufSize);
        randBufCur = randBuf;

    };
    if ((randBufCur - randBuf) < 1) {
        randBufCur += syscall(SYS_getrandom, randBuf, bufSize - (randBufCur - randBuf), 0) / sizeof(unsigned int);
    }
    return abs(*(--randBufCur));
}
*/
typedef struct {
    int x;
    int y;
} Pos;

int posEq(Pos a, Pos b){
    return (a.x == b.x) && (a.y == b.y);
}

int posEqPtr(Pos* a,Pos* b) {
    return posEq(*a,*b);
}
typedef enum {
    RelLeft = -1,
    RelStraight = 0,
    RelRight = 1
} RelDirection;

typedef struct Decision {
    int viewSize;
    char* vals;
    RelDirection des;
} Decision;

typedef struct  {
    int width;
    int height;
    int maxPlayers;
}playingField;

typedef struct {
    size_t size;
    size_t used;
    Pos* elems;
    Decision* dess;
}Stack;

Stack* initStack(size_t initalSize){
    Stack* res = malloc(sizeof(Stack));
    if (res == NULL) return NULL;
    res->elems = malloc(sizeof(Pos) * initalSize);
    res->dess = malloc(sizeof(Decision) * initalSize);
    if (res->elems == NULL) {
        free(res);
        return NULL;
    }
    if (res->dess == NULL) {
        free(res);
        return NULL;
    }
    res->size = initalSize;
    res->used = 0;
    return res;
}
void resetStack(Stack* s){
    //res->elems = malloc(sizeof(Pos) * initalSize);
    //res->dess = malloc(sizeof(Decision) * initalSize);
    int i;
    for (i =0;i < s->used;i++) {
        free(s->dess[i].vals);
        pointMap[s->elems[i].x * 1000 + s->elems[i].y] = 0;
    }

    memset(s->elems,0,sizeof(Pos) * s->size);
    memset(s->dess,0,sizeof(Decision) * s->size);
    s->used = 0;
}

void stackPush(Stack* s,Pos p,Decision d){
    if(s->size == s->used) {
        s->elems = realloc(s->elems ,s->size * 2 * sizeof(Pos));
        s->dess = realloc(s->dess ,s->size * 2 * sizeof(Decision));

        s->size *= 2;
        /*Add Null Check */
    }
    s->elems[(s->used)] = p;
    s->dess[(s->used)] = d;
    s->used++;
}

Pos stackPop(Stack* s){
    return s->elems[s->used--];
}

int posInStack(Stack* s,Pos p){
    int i;
    int sum = 0;
    for (i = 0;i < s->used;i++){
        Pos c = (s->elems[i]);
        sum += posEq(c,p);
    }
    return sum;
}

typedef enum {
    Left = 0,
    Top = 1,
    Right = 2,
    Down = 3
} AbsDirection;

AbsDirection applyRel(AbsDirection a,RelDirection r) {
    return (a + r) % 4;
}


typedef struct {
    AbsDirection firstDir;
    int id;
    Pos curPos;
    AbsDirection dir;
    int alive;
    Stack* way;
    int points;
    int lives;
} Snake;

typedef struct {
    Snake** snakes;
    int snakeCount;
} Snakepit;

int posInSnakepit(Snakepit* sp,Pos p) {
    if (p.x <= 0 || p.x >= 999 || p.y <= 0 || p.y >= 999) return 1;
    return pointMap[p.x * 1000 + p.y] | overlay[p.x * 1000 + p.y];
}

Snake* initSnake(int id) {
    Snake* s = malloc(sizeof(Snake));
    s->points =0;
    s->lives = 0;
    s->id = id;
    s->way = initStack(10);
    if (s->way == NULL) exit(1);
    Pos p =(Pos) {  500 - spawnDistance + 2 * rand() % ( spawnDistance * 2),500 - spawnDistance + 2 * rand() % ( spawnDistance * 2)};
    s->curPos = (Pos) {  500 - spawnDistance + 2 * rand() % ( spawnDistance * 2),500 - spawnDistance + 2 * rand() % ( spawnDistance * 2)};
    s->dir = rand() % 4;
    s->firstDir = s->dir;
    s->alive = 1;
    return s;
}

void resetSnake(Snake* s,Snakepit* sp) {
    s->points += s->way->used;
    s->lives++;
    resetStack(s->way);
    s->curPos = (Pos) {  500 - spawnDistance + 2 * rand() % ( spawnDistance * 2),500 - spawnDistance + 2 * rand() % ( spawnDistance * 2)};
    while (posInSnakepit(sp,s->curPos)) {
        s->curPos = (Pos) {  500 - spawnDistance + 2 * rand() % ( spawnDistance * 2),500 - spawnDistance + 2 * rand() % ( spawnDistance * 2)};

    }
    s->dir = rand() % 4;
    s->firstDir = s->dir;
    s->alive = 1;
}



Pos moveDir(Pos p, AbsDirection d){
    if (p.x <= 0 || p.x >= 999 || p.y <= 0 || p.y >= 999) return  (Pos) {-1,-1};
        switch (d) {
            case Left:
                return (Pos) { (p.x) - 1,(p.y)};
            case Right:
                return (Pos) {(p.x) + 1,(p.y)};
            case Top:
                return (Pos) {(p.x), (p.y) - 1};
            case Down:
                return (Pos) {(p.x), (p.y) + 1};
        }
}



void snakeChangeDirection(Snake* s) {
    s->dir = (s->dir + rand() % 3 - 1) % 4;
}

int posInSnake(Snake* s,Pos p){
    if(posEq((s->curPos),p)) {
        return 1;
    }
    else {
        return posInStack(s->way, p);
    }
}



Snakepit* fillSnakepit(int snakeCount){
    Snakepit* res = malloc(sizeof(Snakepit));
    if (res == NULL) return NULL;
    res->snakes = malloc(sizeof(Snake*) * snakeCount);
    res->snakeCount = snakeCount;
    int i;
    for (i = 0; i < res->snakeCount;i++) {
        res->snakes[i] = initSnake(i);
        snakeChangeDirection(res->snakes[i]);

    }
    return res;
};



Pos translateDir(Pos p, AbsDirection d) {
    Pos res;
    switch (d) {
        case Left: //Works
            return (Pos) {p.y,p.x};
        case Right: //Works
            return (Pos) {-p.y,p.x};
        case Top:
            return (Pos) {p.x,p.y};
        case Down:
            return (Pos) {p.x,-p.y};
    }
}



Decision* initDecision(int size) {
    //size++;
    Decision* res = malloc(sizeof(Decision));
    res->viewSize = size;
    res->vals = calloc(sizeof(char),(size) * (size));
    //res->vals = malloc(sizeof(char) * size * size);
    //memset(res->vals,0,size*size*sizeof(char));
    return res;
}

void freeDecision(Decision* d) {
    free(d->vals);
    free(d);
}

void setPointDecision(Decision* d,int x, int y) {
    if (x >= d->viewSize || y >= d->viewSize) exit(1);
    d->vals[y* d->viewSize + x] = 1;
}

char getPointDecision(Decision* d,int x, int y) {
    if (x >= d->viewSize || y >= d->viewSize) exit(2);
    return d->vals[y* (d->viewSize)  + x];
}

void trainOnDecisionBad(Decision* d,int nnID) {
    double input[nnInputSize];
    int i = 0;
    for (i = 0;i < nnInputSize;i++){
        input[i] = d->vals[i];
    }
    double output[3];
    output[0] = 1;
    output[1] = 1;
    output[2] = 1;
    if (d->des == -1) output[0] = 0;
    if (d->des == 0) output[1] = 0;
    if (d->des == 1) output[2] = 0;
    genann_train(ann[nnID], input, output, (double ) learnigrate / 100.0 );
}

void trainOnDecisionGood(Decision* d,int nnID) {
    double input[nnInputSize];
    int i = 0;
    for (i = 0;i < nnInputSize;i++){
        input[i] = d->vals[i];
    }
    double output[3];
    output[0] = 0;
    output[1] = 0;
    output[2] = 0;
    if (d->des == -1) output[0] = 1;
    if (d->des == 0) output[1] = 1;
    if (d->des == 1) output[2] = 1;
    genann_train(ann[nnID], input, output,(double ) learnigrate / 100.0 );
}

int findMax(double* arr,int len){
    double max = arr[0];
    int maxPos = 0;
    int i;
    for (i = 0;i < len;i++){
        if (arr[i] > max) {
            max = arr[i];
            maxPos = i;
        }
    }
    return maxPos;
}

int decideOnDecision(Decision* d,int nnID) {
    double input[nnInputSize];
    int i = 0;
    for (i = 0;i < nnInputSize;i++){
        input[i] = d->vals[i];
    }
    const double* output;
    output = genann_run(ann[nnID], input);
    return findMax(output,3) -1;

    /*
    if (*output > 0.5) {
        return 1;
    } else {
        return 0;
    }
     */
}

void policy(Snakepit* sp,int snakeID,Decision** d){
    /* Render Snakevision */


    *d = initDecision(viewLen*2+1);

    int minX = 0;
    int maxX = 2 * viewLen;
    int minY = 0;
    int maxY = 2* viewLen;

    int i;
    int j;
    for (i = minX;i <= maxX;i++){
        for (j = minY;j <= maxY;j++){
            Pos p = sp->snakes[snakeID]->curPos;
            Pos viewos = (Pos) {i - viewLen,j - viewLen};
            viewos = translateDir(viewos,sp->snakes[snakeID]->dir);
            p.x += viewos.x;
            p.y += viewos.y;
            if(posInSnakepit(sp,p)) {
               setPointDecision(*d,i, j );
            }
        }
    }
    (*d)->des = decideOnDecision(*d,snakeID);
}

void stepSnakepit(Snakepit* sp){
    int i;
    for (i = 0; i < sp->snakeCount;i++) {
        Snake* s = sp->snakes[i];
        if (sp->snakes[i]->alive > 0) {
            Pos nextPos;
            Decision* d;
            policy(sp,i,&d);
            nextPos = moveDir(s->curPos,s->dir);
            if(posInSnakepit(sp,nextPos) || (nextPos.x <= 0 || nextPos.x >= 999 || nextPos.y <= 0 || nextPos.y >= 999) ) {
                SnakesLivedAll++;
                s->alive = - respwanDelay;
                if ( s->way->used > longestSnake) longestSnake = s->way->used;
            } else{
                /*
                Pos p = sp->snakes[i]->way->elems[0];
                AbsDirection a = sp->snakes[i]->firstDir;
                int j = 0;
                for (j = 0; j < s->way->used;j++){
                    a = applyRel(a,s->way->dess[j].des);
                    p = moveDir(p,a);
                    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
                    SDL_RenderDrawPoint(renderer, p.x, p.y);
                }
                 */
            }
            sp->snakes[i]->dir = applyRel(sp->snakes[i]->dir,d->des);
            sp->snakes[i]->curPos = nextPos;
            stackPush(sp->snakes[i]->way,nextPos,*d);
            pointMap[nextPos.x * 1000 + nextPos.y] = 1;
            free(d);
        }
        if (s->alive <= 0 ){
            s->alive++;
            if (s->alive > 0) {
                resetSnake(sp->snakes[i],sp);
            } else {
                //int split = (s->way->used * 80) / 100 ;
                int split = s->way->used - splitPoint;
                Pos p = sp->snakes[i]->way->elems[0];
                AbsDirection a = sp->snakes[i]->firstDir;
                int j = 0;
                for (j = 0; j < split;j++){
                    if (s->alive == (-respwanDelay + 1)) {
                        if (gLearnLimit > 0) {
                            if (j % (int ) (s->way->used / gLearnLimit + 1) == 0) {
                                trainOnDecisionGood(&(s->way->dess[j]), i);
                            }
                        }
                        else {
                                trainOnDecisionGood(&(s->way->dess[j]),i);
                        }
                    }

                    a = applyRel(a,s->way->dess[j].des);
                    p = moveDir(p,a);
                    if (showBodys) {
                        SDL_SetRenderDrawColor(renderer, 124, 252, 0, 255);
                        SDL_RenderDrawPoint(renderer, p.x, p.y);
                    }
                }
                for (j = split; j < s->way->used;j++){
                    if (s->alive == (- respwanDelay + 1)) {
                        trainOnDecisionBad(&(s->way->dess[j]),i);
                    }
                    a = applyRel(a,s->way->dess[j].des);
                    p = moveDir(p,a);
                    if (showBodys) {
                        SDL_SetRenderDrawColor(renderer, 204, 0, 0, 255);
                        SDL_RenderDrawPoint(renderer, p.x, p.y);
                    }
                }
            }


        }

    }
}

void renderOverlay() {
    int i;
    for (i = 0;i < 1000000;i++) {
        if (overlay[i]) {
            SDL_SetRenderDrawColor(renderer, 100, 100, 100, 255);
            SDL_RenderDrawPoint(renderer, i / 1000, i % 1000);
        }
    }
}
void renderPointmap() {
    int i;
    for (i = 0;i < 1000000;i++) {
        if (pointMap[i]) {
            SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
            SDL_RenderDrawPoint(renderer, i / 1000, i % 1000);
        }
    }
}

int argsInt(char* longname, char* shortname, int def, int argc,char** args ){
    int i = 0;
    int res = def;
    for (i = 0;i < argc;i++) {
        if (strcmp(longname,args[i]) == 0 || strcmp(shortname,args[i]) == 0 ) {
            res = strtol(args[i+1],NULL,10);
        }
    }
    return res;
}

double filledPercent() {
    int filled = 0;
    int i = 0;
    for (i = 0; i < 1000000;i++){
        filled += pointMap[i];
    }
    return ((double) filled / (double) 1000000) * 100.;
}

double avgSnakelen(Snakepit* sp) {
    int i;
    int accLen = 0;
    for (i = 0; i < sp->snakeCount;i++) {
        accLen += sp->snakes[i]->way->used;
    }
    return (double) accLen / (double) sp->snakeCount;
}

void snakeSelect(Snakepit* sp) {
    int i;
    for (i = 0;i < sp->snakeCount;i++) {
        printf("Snake No. %d %d points in %d lives. %lf\n",i,sp->snakes[i]->points,sp->snakes[i]->lives,
               (double) sp->snakes[i]->points / (double) sp->snakes[i]->lives);
    }
}


#define clear() printf("\033[H\033[J")
int main(int argc,char** args) {
    int snakeCount;
    snakeCount = argsInt("--snake-count","-sc",50,argc,args);
    viewLen = argsInt("--view-len","-vl",3,argc,args);
    nnInputSize = (viewLen * 2 + 1) * (viewLen * 2 + 1) ;
    nnLayerCount = argsInt("--layer-count","-lc",1,argc,args);
    nnLayerSize =  argsInt("--layer-size","-lc",20,argc,args);
    seed = argsInt("--seeds","-s",5,argc,args);
    spawnDistance = argsInt("--spawn-distance","-sd",500,argc,args);
    splitPoint = argsInt("--split-point","-sp",viewLen,argc,args);
    respwanDelay = argsInt("--respwan-delay","-rd",10,argc,args);
    renderEvery = argsInt("--render-every","-re",1,argc,args);
    gLearnLimit = argsInt("--glearn-limit","-gl",10,argc,args);
    learnigrate = argsInt("--learning-rate","-lr",300,argc,args);
    showBodys = argsInt("--show-bodys","-sb",0,argc,args);

    createChessOverlay();
    setvbuf(stdout, NULL, _IONBF, 0);
    SDL_Event event;
    SDL_Window *window;
    int i;
    srand(seed);

    memset(pointMap,0,1000000);

    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(WINDOW_WIDTH, WINDOWS_HEIGHT, 0, &window, &renderer);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);
    SDL_Delay(500);
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    const Uint8 *keys = SDL_GetKeyboardState(NULL);
    ann = malloc(sizeof(genann*)  * snakeCount);
    for (i = 0;i < snakeCount;i++) {
        ann[i] = genann_init(nnInputSize, nnLayerCount, nnLayerSize, 3);
        genann_randomize(ann[i]);
    }
    Snakepit* sp = fillSnakepit(snakeCount);
    while (1) {
        stepSnakepit(sp);
        //renderSP(sp,renderer);
        if (fc % renderEvery == 0 ){
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
            SDL_RenderClear(renderer);

            renderOverlay();
            renderPointmap();
            SDL_RenderPresent(renderer);
            //SDL_Delay(1);

        }
        fc++;
        if (fc % 1000 == 0) {
            clear();
            printf("Step: %d SneakLivedAll: %d\n", fc,SnakesLivedAll);
            printf("Filled: %lf\n",filledPercent());
            printf("Longest Snake: %d\n",longestSnake);
            printf("Averge Snakelen: %lf\n",avgSnakelen(sp));
            snakeSelect(sp);

        }
        //if(fc == 10000) break;

        if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
            break;

    }



    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS;
}