cmake_minimum_required(VERSION 3.9)
project(NeuralSnake)
find_package(SDL2 REQUIRED)
include_directories(${SDL2_INCLUDE_DIRS})


set(CMAKE_CXX_STANDARD 11)

add_executable(NeuralSnake
        genann.c
        genann.h
        main.c)

target_link_libraries(NeuralSnake ${SDL2_LIBRARIES} m)
